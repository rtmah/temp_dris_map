//This entire file should be replaced by Vince's Service
(function () {


function geojsonParser ($http) {

  var eventsgeojson = {"type":"FeatureCollection","features":[]};
  var EarthquakeJSON = {};

  $http.get("http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=NOW-30days&endtime=NOW&alertlevel=green")
  .then(function(response) {
    if (response.data.features.length > 0)
      filterUSGSdata(response.data,eventsgeojson.features);
    else
      console.log("no yellow earthquakes")
  });

  $http.get("http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=NOW-30days&endtime=NOW&alertlevel=orange")
  .then(function(response) {
    if (response.data.features.length > 0)
      filterUSGSdata(response.data,eventsgeojson.features);
    else
      console.log("no orange earthquakes")
  });

  $http.get("http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=NOW-30days&endtime=NOW&alertlevel=red")
  .then(function(response) {
    if (response.data.features.length > 0)
      filterUSGSdata(response.data,eventsgeojson.features);
    else
      console.log("no red earthquakes")
  });

  this.getGeojson = function () {
    return eventsgeojson;
  };

};

function filterUSGSdata (originaldata,eventsgeojson) {
  for (var i = 0; i < originaldata.features.length; i++){

    var filtered = {"type": "Feature", "geometry": {"type":"Point","coordinates":[]}, "properties": {}};
    var original = originaldata.features[i];

    filtered.geometry.coordinates.push(original.geometry.coordinates[0]);
    filtered.geometry.coordinates.push(original.geometry.coordinates[1]);
    filtered.properties = {"description":original.properties.title,
                           "url":original.properties.url,
                           "etype": "earthquake",
                          };
    eventsgeojson.push(filtered);
  };
};


mapApp.service("geojsonParser", ['$http', geojsonParser]);

})();
