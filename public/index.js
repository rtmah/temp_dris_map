  var mapApp = angular.module("mapDRISapp", ["ngRoute","leaflet-directive"]);

  //router for webpage
  mapApp.config(function ($routeProvider) {
    $routeProvider
      .when("/all_events", {controller: "EventListController", service:"geojsonParser", templateUrl: "partials/event_list_partial.html"})
      .when("/", { redirectTo: "/all_events" })
      .when("/404_page", { controller: "Controller404", templateUrl: "partials/404_page_partial.html" })
      .otherwise( { redirectTo: "/404_page" });
  
  });
