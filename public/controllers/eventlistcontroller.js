(function () {

//create controller for angular
mapApp.controller('EventListController', ['$scope', 'geojsonParser', EventListController]);

//define controller
function EventListController ($scope, geojsonParser) {

//TODO: Parse from Vince's Service, will require an edit below as well
  events_geojson = geojsonParser.getGeojson();

//TODO: Move access token
  accessToken = "pk.eyJ1IjoicnRtYWgiLCJhIjoiY2lqZXZvMW9jMDAwb3RzbHRwcHR5ZzMzMSJ9.Macc7Sh03lmB-Psdes9fGA";

//Creates tile layer json
  var mapbox = { 
                 url: "https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token={accessToken}",
               options: {
                 attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>',
                 id: 'rtmah.p2ce89mf',
                 accessToken: accessToken,
               },
  };

//TODO: come up with a more permentant palce
  //Creates Marker points in Geojsonfile for earthquakes perhaps it can be in CSS
  var earthquakeMarker = L.icon({"iconUrl":"../img/earthquake-3.png",
     "iconSize":[30,40],
     "iconanchor":[15,39],
     "popupAnchor":[31,0],
    });
  
  //Will be moved to make a more permanent function for all marker setups
  //This function makes marker layer for a give geojson point feature
  function makeEarthquakeMarkers(feature,latlng) {
    marker =  L.marker(latlng, {"icon": earthquakeMarker})
              //.bindPopup creates Popup for layer (or marker in this case)
               .bindPopup("<a href="+feature.properties.url+">"+feature.properties.description+"</a>");
    return marker
  };

  //Passes leaflet inputs to html through angular scope
  angular.extend($scope, {
                   geojson: {"data": events_geojson.features,
                             "options":{pointToLayer:makeEarthquakeMarkers,
                             }
                            }, //passes converted to layer jsonfile
                   defaults: {scrollWheelZoom: false}, //REMOVE when done debugging
                   tiles: mapbox,  //baselayer (satellites, streetsviewer, etc.)
                   startlocation: { //sets starting location
                     lat: 40,
                     lng: -100,
                     zoom: 4, 
                   },
  });


};

})();
